const mongoose = require('mongoose')

//Schema

const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		default: 'Pending'
	}
})

module.exports = mongoose.model('Task', taskSchema)