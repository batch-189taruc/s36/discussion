//controllers naming convention must be PascalCase

const Task = require('../models/Task.js')

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

//this is for register

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name})

	return newTask.save().then((savedTask,error) => {
		if(error){
			return error
		}

		return 'Task created successfully!'
	})
}

// this is for update

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}
		result.name = newContent.name
		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}

			return updatedTask
		})
	})
}

//this is for delete

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error){
			return error
		}
		return deletedTask
	})
}


//this is for find
module.exports.findTask = (taskId) =>{
	return Task.findById(taskId).then((foundTask, error) => {
		if(error){
			return error
		}
		return foundTask
	})
}