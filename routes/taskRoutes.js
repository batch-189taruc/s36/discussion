const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

router.get('/',(req,res) => {
	//business logic here
	TaskController.getAllTasks().then((tasks) => res.send(tasks))

})
router.post('/create', (req,res) => {
	TaskController.createTask(req.body).then((task) => res.send(task))
})

router.put('/:id/update', (req,res) =>{
	TaskController.updateTask(req.params.id, req.body).then((updatedTask) => 
		res.send(updatedTask))
})

router.delete('/:id/delete', (req,res) =>{
	TaskController.deleteTask(req.params.id).then((deletedTask) => res.send(deletedTask))
	
})

router.get('/:id/find', (req,res) =>{
	TaskController.findTask(req.params.id).then((foundTask) => res.send(foundTask))
})
module.exports = router